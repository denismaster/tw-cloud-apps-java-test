package cloud.timeweb.cloudapps.javatest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudAppsTestApplication {
	public static void main(String[] args) {
		SpringApplication.run(CloudAppsTestApplication.class, args);
	}
}
